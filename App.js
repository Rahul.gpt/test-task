import React, {Component} from 'react';
import {View, Text} from 'react-native';
import AddEmployeeButton from './src/Components/employeeList/index';
import Navigator from './src/Components/navigations/switchNavigator/index';

export default class App extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Navigator />
      </View>
    );
  }
}
