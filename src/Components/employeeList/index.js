import React, {Component} from 'react';
import {
  View,
  Text,
  AsyncStorage,
  SafeAreaView,
  Image,
  TouchableOpacity,
  FlatList,
  Modal,
  TouchableWithoutFeedback,
  Platform,
  StyleSheet,
} from 'react-native';
import Menu from '../../Images/menu.png';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Verticle from '../../Images/verticle.png';
import Star from '../../Images/star.png';
import StarFill from '../../Images/star_1.png';
import 'react-native-gesture-handler'

export default class EmployeeList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employeeList: [],
      modalVisible: false,
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('employeeLists').then(data => {
      let employee = JSON.parse(data);
      if (employee) {
        this.setState({employeeList: employee});
      }
    });
  }

  toggle = index => {
    let employeeList = [...this.state.employeeList];
    employeeList[index].favorite = !employeeList[index].favorite;
    this.setState({employeeList}, () => {
      AsyncStorage.removeItem('employeeLists');
      AsyncStorage.setItem(
        'employeeLists',
        JSON.stringify(this.state.employeeList),
      );
      AsyncStorage.getItem('employeeLists').then(data => {
        let employee = JSON.parse(data);
        this.setState({employeeList: employee});
      });
    });
  };

  addEmployee = () => {
    this.setState({modalVisible: false}, () => {
      this.props.navigation.navigate('AddEmployee');
    });
  };

  sortEmployee = () => {
    let employeeList = [...this.state.employeeList];
    employeeList.sort(function(a, b) {
      var nameA = a.fname.toLowerCase(),
        nameB = b.fname.toLowerCase();
      if (nameA > nameB) {
        return -1;
      } else {
        return 1;
      }
      return 0;
    });
    this.setState({employeeList: employeeList, modalVisible: false});
  };

  render() {
    return (
      <SafeAreaView style={styles.safeAreaContainer}>
        <View style={styles.mainContainer}>
          <View style={styles.mainHeaderContainer}>
            <View style={styles.menuImageContainer}>
              <TouchableOpacity
                onPress={() => this.props.navigation.toggleDrawer()}>
                <Image
                  source={Menu}
                  style={styles.menuImageStyle}
                  tintColor="#000"
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
            <View style={styles.headerTextContainer}>
              <Text style={styles.headerText}>Inbox</Text>
            </View>
            <View style={styles.fabIconContainer}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({modalVisible: !this.state.modalVisible})
                }>
                <Image
                  source={Verticle}
                  style={styles.fabIconStyle}
                  tintColor="#000"
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <Modal
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                presentationStyle="overFullScreen"
                onRequestClose={() => {
                  this.setState({modalVisible: false});
                }}>
                <TouchableWithoutFeedback
                  onPress={() => this.setState({modalVisible: false})}>
                  <View style={styles.modalMainContainer}>
                    <View elevation={5} style={styles.modalInnerContainer}>
                      <View
                        style={{
                          alignItems: 'center',
                        }}>
                        <TouchableOpacity
                          onPress={this.addEmployee}
                          style={styles.modalContainer}>
                          <View style={styles.modalTextContainer}>
                            <Text>Add Employee</Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.modalContainer}
                          onPress={this.sortEmployee}>
                          <View style={styles.modalTextContainer}>
                            <Text>Sort Employees</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </Modal>
            </View>
          </View>
          <View style={{alignItems: 'center'}}>
            <View style={{width: wp('95%')}}>
              <FlatList
                data={this.state.employeeList}
                renderItem={({item, index}) => {
                  return (
                    <View style={{alignItems: 'center'}}>
                      <View elevation={5} style={styles.listContainer}>
                        <View style={{width: '25%', alignItems: 'center'}}>
                          <View style={styles.circleAvtarConatiner}>
                            {item.fname && item.lName ? (
                              <Text
                                style={{
                                  fontWeight: 'bold',
                                  fontSize: hp('2%'),
                                }}>
                                {item.fname.charAt(0) + item.lName.charAt(0)}
                              </Text>
                            ) : null}
                          </View>
                        </View>
                        <View style={{width: '50%'}}>
                          <View style={{paddingBottom: hp('1%')}}>
                            <Text
                              style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                              {item.fname + ' ' + item.lName}
                            </Text>
                          </View>
                          <View>
                            <Text style={{color: 'grey'}}>{item.jobTitle}</Text>
                          </View>
                        </View>
                        <View style={styles.favIconContainer}>
                          {item.favorite ? (
                            <TouchableOpacity
                              onPress={() => this.toggle(index)}>
                              <Image
                                source={StarFill}
                                style={{width: 30, height: 30}}
                                resizeMode="contain"
                              />
                            </TouchableOpacity>
                          ) : (
                            <TouchableOpacity
                              onPress={() => this.toggle(index)}>
                              <Image
                                source={Star}
                                style={{
                                  width: 30,
                                  height: 30,
                                  tintColor: 'black',
                                }}
                                resizeMode="contain"
                                tintColor="black"
                              />
                            </TouchableOpacity>
                          )}
                        </View>
                      </View>
                    </View>
                  );
                }}
              />
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeAreaContainer: {flex: 1},
  mainContainer: {flex: 1, marginBottom: hp('10%')},
  mainHeaderContainer: {
    width: wp('100%'),
    height: hp('10%'),
    backgroundColor: '#1bab5c',
    flexDirection: 'row',
    alignItems: 'center',
  },
  menuImageContainer: {
    width: wp('20%'),
    alignItems: 'center',
    paddingVertical: hp('2%'),
  },
  menuImageStyle: {width: 30, height: 20, tintColor: '#000'},
  headerTextContainer: {width: wp('60%'), alignItems: 'center'},
  headerText: {fontSize: hp('3%'), fontWeight: 'bold'},
  fabIconContainer: {
    width: wp('20%'),
    alignItems: 'center',
    paddingVertical: hp('2%'),
  },
  fabIconStyle: {width: 30, height: 30, tintColor: '#000'},
  modalMainContainer: {
    justifyContent: 'flex-start',
    flex: 1,
    alignItems: 'flex-end',
  },
  modalInnerContainer: {
    width: wp('50%'),
    height: hp('16%'),
    backgroundColor: 'white',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 0.5},
    shadowOpacity: 0.5,
    shadowRadius: 1,
    top: Platform.OS === 'ios' ? hp('13%') : hp('8%'),
    right: wp('2%'),
  },
  modalContainer: {
    height: hp('8%'),
    justifyContent: 'center',
    width: '100%',
  },
  modalTextContainer: {paddingLeft: wp('4%')},
  listContainer: {
    width: '95%',
    marginVertical: hp('2%'),
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 0.5},
    shadowOpacity: 0.5,
    shadowRadius: 1,
    backgroundColor: '#fff',
    borderRadius: 6,
    paddingVertical: 10,
  },
  circleAvtarConatiner: {
    width: 60,
    height: 60,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1bab5c',
  },
  favIconContainer: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
