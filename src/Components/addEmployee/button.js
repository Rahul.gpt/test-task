import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import navigator from '../navigations/switchNavigator/index'

export default class AddEmployeeButton extends Component {
  render() {
    return (
      <SafeAreaView style={styles.safeAreaViewContainer}>
        <View style={styles.mainContainer}>
          <View style={styles.buttonMainContainer}>
            <TouchableOpacity
              style={styles.buttonStyle}
              onPress={() => this.props.navigation.navigate('addEmployeeForm')}>
              <View>
                <Text style={styles.buttonTextStyle}>add employee</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeAreaViewContainer: {
    flex: 1,
  },
  mainContainer: {
    backgroundColor: '#1bab5c',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonMainContainer: {
    width: wp('80%'),
    alignItems: 'center',
    backgroundColor: '#008b48',
    paddingVertical: 15,
    borderRadius: 5,
  },
  buttonStyle: {
    width: '100%',
    alignItems: 'center',
  },
  buttonTextStyle: {
    color: 'white',
    textTransform: 'uppercase',
    fontSize: 20,
    fontWeight: 'bold',
  },
});
