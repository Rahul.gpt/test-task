import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Keyboard,
  AsyncStorage,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

export default class AddEmployee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fName: '',
      lName: '',
      jobTitle: '',
      salary: '',
      employeeDetails: [],
      firstNameValidBool : true,
      lastNameValidBool:true,
      jobTitleValidBool : true
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('employeeLists').then(data => {
      let employee = JSON.parse(data);
      if (employee) {
        this.setState({employeeDetails: employee});
      }
    });
  }

  saveData = () => {
    let {fName, lName, jobTitle, salary} = this.state;
    let employeeDetails = [...this.state.employeeDetails];
    if (!fName || !lName || !jobTitle || !salary) {
      alert('please fill all details');
    } else {
      employeeDetails.push({
        fname: fName,
        lName: lName,
        jobTitle: jobTitle,
        salary: salary,
        favorite: false,
      });

      this.setState({employeeDetails}, () => {
        AsyncStorage.setItem(
          'employeeLists',
          JSON.stringify(this.state.employeeDetails),
        );
        this.props.navigation.navigate('EmployeeList');
      });
    }
  };

  validateFirstName = () => {
    let rs;
    rs = /^[a-zA-Z ]+$/;
    if (rs.test(this.state.fName) === false) {
      this.setState({
        fName: '',
        firstNameValidBool: false
      });
    } else {
      this.setState({firstNameValidBool: true});
    }
  };

  validateLastName = () => {
    let rs;
    rs = /^[a-zA-Z ]+$/;
    if (rs.test(this.state.lName) === false) {
      this.setState({
        lName: '',
        lastNameValidBool: false,
      });
    } else {
      this.setState({lastNameValidBool: true});
    }
  };

  jobDetailValidate = () => {
    let rs;
    rs = /^[a-zA-Z ]+$/;
    if (rs.test(this.state.jobTitle) === false) {
      this.setState({
        lName: '',
        jobTitleValidBool: false,
      });
    } else {
      this.setState({jobTitleValidBool: true});
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.safeAreaViewContainer}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <View style={{marginVertical: hp('5%')}}>
            <Text style={{color: '#9bc88c', fontSize: hp('4%')}}>
              Enter employee details
            </Text>
          </View>
          <KeyboardAwareScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
            <View style={{width: wp('80%')}}>
              <View>
                <View>
                  <Text style={styles.inputDataType}>first name</Text>
                </View>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.inputStyle}
                    onChangeText={e => this.setState({fName: e})}
                    onEndEditing={this.validateFirstName}
                    value={this.state.fName}
                  />
                </View>
                <View>
                  {
                    this.state.firstNameValidBool ? null : <Text style={{color:'red'}}>* First name is wrong</Text>
                  }
                </View>
              </View>
              <View style={styles.marginInputViews}>
                <View>
                  <Text style={styles.inputDataType}>last name</Text>
                </View>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.inputStyle}
                    onChangeText={e => this.setState({lName: e})}
                    onEndEditing={this.validateLastName}
                    value={this.state.lName}
                  />
                </View>
                <View>
                  {
                    this.state.lastNameValidBool ? null : <Text style={{color:'red'}}>* last name is wrong</Text>
                  }
                </View>
              </View>
              <View>
                <View>
                  <Text style={styles.inputDataType}>job title</Text>
                </View>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.inputStyle}
                    onChangeText={e => this.setState({jobTitle: e})}
                    value={this.state.jobTitle}
                    onEndEditing={this.jobDetailValidate}
                  />
                </View>
                {/* <View>
                  {
                    this.state.jobTitleValidBool ? null : <Text style={{color:'red'}}>* last name is wrong</Text>
                  }
                </View> */}
              </View>
              <View style={styles.marginInputViews}>
                <View>
                  <Text style={styles.inputDataType}>salary</Text>
                </View>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.inputStyle}
                    onChangeText={e => this.setState({salary: e})}
                    keyboardType="numeric"
                  />
                </View>
              </View>
              <View style={styles.buttonMainContainer}>
                <TouchableOpacity
                  style={styles.buttonStyle}
                  onPress={this.saveData}>
                  <View>
                    <Text style={styles.buttonTextStyle}>save</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeAreaViewContainer: {
    flex: 1,
  },
  buttonMainContainer: {
    width: wp('80%'),
    alignItems: 'center',
    backgroundColor: '#008b48',
    paddingVertical: 15,
    borderRadius: 5,
  },
  buttonStyle: {
    width: '100%',
    alignItems: 'center',
  },
  buttonTextStyle: {
    color: 'white',
    textTransform: 'uppercase',
    fontSize: 20,
    fontWeight: 'bold',
  },
  inputDataType: {
    textTransform: 'capitalize',
  },
  inputContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#9bc88c',
  },
  inputStyle: {
    // height: hp('6%'),
    fontSize: hp('3%'),
  },
  marginInputViews: {marginVertical: hp('4%')},
});
