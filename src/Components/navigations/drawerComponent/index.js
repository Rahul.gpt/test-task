import React, {Component} from 'react';
import {View, Text, AsyncStorage, SafeAreaView, StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default class DrawerContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalEmployees: 0,
      totalFavoriteEmployees: 0,
      employeeLists: [],
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('employeeLists').then(data => {
      let employees = JSON.parse(data);
      if (employees) {
        this.setState({employeeLists: employees});
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    AsyncStorage.getItem('employeeLists').then(data => {
      let updateList = JSON.parse(data);
      if (
        JSON.stringify(updateList) !== JSON.stringify(this.state.employeeLists)
      ) {
        this.setState({employeeLists: updateList});
      }
    });
    return true;
  }

  render() {
    let totalEmployees = 0;
    let totalFavoriteEmployees = 0;
    totalEmployees = this.state.employeeLists.length;
    this.state.employeeLists.map(data => {
      if (data.favorite) {
        totalFavoriteEmployees += 1;
      }
    });
    return (
      <SafeAreaView style={styles.safeAreaContainer}>
        <View style={styles.container}>
          <View>
            <View>
              <Text style={styles.textStyle}>
                {'Total number of employees :' + totalEmployees}
              </Text>
            </View>
            <View>
              <Text style={styles.textStyle}>
                {'Total number of favorite employees :' +
                  totalFavoriteEmployees}
              </Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
  },
  container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  textStyle: {
    fontSize: hp('2.5%'),
    fontWeight: 'bold',
  },
});
