import React, { Component } from "react";
import {
  createAppContainer,
  Dimensions,
  DrawerItems,
  createStackNavigator,
  StyleSheet
} from "react-navigation";
import { createDrawerNavigator } from 'react-navigation-drawer'
import EmployeeList from '../../employeeList/index';
import DrawerContent from '../drawerComponent/index'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
const RootDrawer = createDrawerNavigator(
  {
    // YourMood: { screen: YourMood },
    EmployeeList: { screen: EmployeeList }
  },
  {
    contentComponent: DrawerContent,
    // drawerWidth: 300,
    drawerWidth: wp("70%"),
    drawerPosition: "left",
    drawerBackgroundColor: "#1bab5c"
    // drawerOpenRoute: 'DrawerOpen',
    // drawerCloseRoute: 'DrawerClose',
    // drawerToggleRoute: 'DrawerToggle',
    // contentOptions: {
    //   activeTintColor: "red",
    //   backgroundColor: "black",
    //   flex: 1
    // }
  }
);
// RootDrawer.navigationOptions = ({ navigation }) => {
//   return { title: "aman", headerLeft: <Text>headerLeft</Text>, headerRight: <Text>headerRight</Text> };
// }

const DrawerNavigation = createAppContainer(RootDrawer);

export default DrawerNavigation;
