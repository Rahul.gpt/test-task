import React, {Component} from 'react';
import {AsyncStorage, View} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import AddEmployeeButton from '../../addEmployee/button';
import AddEmployee from '../../addEmployee/index';
import DrawerNavigation from '../drawerNavigation/index';
import EmployeeList from '../../employeeList/index';

const noData = createStackNavigator({
  AddEmployeeButton: {
    screen: AddEmployeeButton,
    navigationOptions: {header: null},
  },
});

const addEmployeeForm = createStackNavigator({
  AddEmployee: {screen: AddEmployee, navigationOptions: {header: null}},
});

const data = createStackNavigator({
  Dashboard: {screen: DrawerNavigation, navigationOptions: {header: null}},
  AddEmployeeButton: {
    screen: AddEmployeeButton,
    navigationOptions: {header: null},
  },
});

class HandleNavigation extends Component {
  constructor(props) {
    super(props);
    this.checkData();
  }

  checkData = async () => {
    const value = await AsyncStorage.getItem('employeeLists');
    if (value !== null) {
      this.props.navigation.navigate('EmployeeList');
    } else if (value === null) {
      this.props.navigation.navigate('NoData');
    }
  };
  render() {
    return null;
  }
}

export const Route = createSwitchNavigator({
  HandleNavigation: HandleNavigation,
  NoData: {screen: noData},
  EmployeeList: {screen: data},
  addEmployeeForm: {screen: addEmployeeForm},
});
const navigator = createAppContainer(Route);
export default navigator;
